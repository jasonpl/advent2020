from pprint import pprint

heights = []


def look_in_cardinals(z_i, x_i):
    curr_height = heights[z_i][x_i]
    vert = [zv[x_i] for zv in heights]

    # look N
    if max(vert[:z_i]) < curr_height:
        return 1
    # look S
    if max(vert[z_i + 1:]) < curr_height:
        return 1
    # look W
    if max(heights[z_i][:x_i]) < curr_height:
        return 1
    # look E
    if max(heights[z_i][x_i + 1:]) < curr_height:
        return 1
    # not visible from any cardinal!
    return 0


def scan_view(max_height, height_list):
    view_count = 0
    for i in range(len(height_list)):
        view_count += 1
        if height_list[i] >= max_height:
            break
    return view_count


def viewing_score(z_i, x_i):
    curr_height = heights[z_i][x_i]
    vert = [zv[x_i] for zv in heights]

    n, e, s, w = 0, 0, 0, 0
    n = scan_view(curr_height, list(reversed(vert[:z_i])))
    s = scan_view(curr_height, vert[z_i + 1:])
    w = scan_view(curr_height, list(reversed(heights[z_i][:x_i])))
    e = scan_view(curr_height, heights[z_i][x_i + 1:])

    # print(f'n,e,s,w = {n},{e},{s},{w}')
    return n * e * s * w


with open('../inputs/day8.txt') as puzzle_input:
    for line in puzzle_input:
        heights.append(list(map(int, list(line.strip()))))

x_len = len(heights[0])
z_len = len(heights)

# part 1
vis_map = [[0] * x_len for i in range(z_len)]

for z, tree_line in enumerate(heights):
    for x, tree in enumerate(tree_line):
        if x == 0 or x == x_len - 1:  # W/E edges
            vis_map[z][x] = 1
            continue
        elif z == 0 or z == z_len - 1:  # N/S edges
            vis_map[z][x] = 1
            continue
        else:
            # print(f'seeing {z}{x}')
            res = look_in_cardinals(z, x)
            vis_map[z][x] = res

# pprint(heights)
# pprint(vis_map)

print(sum(map(sum, vis_map)))

# part 2
high_score = 0

for z, tree_line in enumerate(heights):
    for x, tree in enumerate(tree_line):
        if x == 0 or x == x_len - 1:  # W/E edges
            continue
        elif z == 0 or z == z_len - 1:  # N/S edges
            continue
        else:
            # print(f'seeing {z}{x}')
            res = viewing_score(z, x)
            high_score = max(res, high_score)

# pprint(heights)

print(high_score)
