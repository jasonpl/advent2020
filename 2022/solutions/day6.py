def n_unique(n):
    with open("../inputs/day6.txt") as fp:
        for line in fp:
            for i in range(len(line) - n):
                if len(set(line[i:i + n])) == n:
                    print(i + n)
                    break


n_unique(4)
n_unique(14)
