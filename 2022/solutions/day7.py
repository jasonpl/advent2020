cwd = ""
fs = {}
ls_mode = False

with open('../inputs/day7.txt') as puzzle_input:
    for line in puzzle_input:
        args = line.strip().split(" ")
        if args[0] == '$':
            if args[1] == 'cd':
                ls_mode = False
                if args[2] == '..':  # up
                    cwd = cwd[:cwd.rfind('/', 0, cwd.rfind('/')) + 1]
                else:  # cd to {dir}
                    if args[2] == '/':  # ugh special case
                        cwd = '/'
                        fs['/'] = 0
                    else:
                        cwd += args[2] + '/'
            elif args[1] == 'ls':
                ls_mode = True
            else:
                print(f'{line} cmd not supported!')
        elif ls_mode:
            if args[0] == 'dir':  # is dir+name
                fs[f'{cwd}{args[1]}'] = 0
            else:  # must be size+filename
                fs[f'{cwd}{args[1]}'] = int(args[0])
        else:
            print(f'what: {line}')

# for path, size in fs.items():
#     print(f'{path} {"dir" if size == -1 else "file, "}{"" if size == -1 else size}')

dir_sizes = {}
for path, size in fs.items():
    if size == 0:
        dir_sizes[path] = 0

for dir_name, dir_size in dir_sizes.items():
    for path, size in fs.items():
        if dir_name in path:
            dir_sizes[dir_name] += size
# print(dir_sizes)

running_count = 0
for dir_name, dir_size in dir_sizes.items():
    if dir_size <= 100_000:
        running_count += dir_size
print(f'p1: {running_count}')

max_size = 70_000_000
min_req_size = 30_000_000
cur_size = dir_sizes['/']
min_del_size = min_req_size - (max_size - cur_size)

sizes = sorted(dir_sizes.values())
for size in sizes:
    if size >= min_del_size:
        print(f'p2: {size}')
        break
