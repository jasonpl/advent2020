instructions = []
reg_hist = []


def get_signal_strength(cycles):
    c = 0
    for cycle in cycles:
        c += cycle * reg_hist[cycle - 1]
        # print(f'{cycle}:{reg_hist[cycle - 1]}')
    return c


with open('../inputs/day8.txt') as fp:
    for line in fp:
        if "noop" in line:
            instructions.append(("noop", 0))
        elif "addx" in line:
            vals = line.split()
            instructions.append((vals[0], int(vals[1])))

pc = 0
reg = 1
for ins, arg in instructions:
    if ins == 'noop':
        reg_hist.append(reg)
        pc += 1
    elif ins == 'addx':
        reg_hist.append(reg)
        reg_hist.append(reg)
        reg += arg
        pc += 2
reg_hist.append(reg)

print(get_signal_strength([20, 60, 100, 140, 180, 220]))

for y in range(6):
    for x in range(40):
        if x - 1 <= reg_hist[y * 40 + x] <= x + 1:
            print("#", end='')
        else:
            print(".", end='')
    print("")
