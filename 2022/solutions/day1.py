f = open("../inputs/day1.txt", "r")
txt = f.read()
temp_input = txt.splitlines()

elf_calories = []

running_calories = 0
for line in temp_input:
    if len(line.strip()) == 0:
        elf_calories.append(running_calories)
        running_calories = 0
    else:
        running_calories += int(line.strip())

print(max(elf_calories))

top_three = 0
for i in range(3):
    x = max(elf_calories)
    top_three += x
    elf_calories.remove(x)

print(top_three)
