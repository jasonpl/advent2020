f = open("../inputs/day2.txt", "r")
txt = f.read()
temp_input = txt.splitlines()

elf_score = 0
hum_score = 0

score_dict = {
    "A X": [3, 3],  # R R
    "A Y": [0, 6],  # R P
    "A Z": [6, 0],  # R S
    "B X": [6, 0],  # P R
    "B Y": [3, 3],  # P P
    "B Z": [0, 6],  # P S
    "C X": [0, 6],  # S R
    "C Y": [6, 0],  # S P
    "C Z": [3, 3],  # S S
}


def find_play(elf_play, desired):
    for play, score in score_dict.items():
        if elf_play in play[0]:
            if desired < 0 and score[0] > score[1]:
                return play
            elif desired == 0 and score[0] == score[1]:
                return play
            elif desired > 0 and score[0] < score[1]:
                return play


# X, Y, Z = L, D, W
def force_round(round):
    if "X" in round:
        # let's lose
        return get_score(find_play(round[0], -1))
    elif "Y" in round:
        # let's draw
        return get_score(find_play(round[0], 0))
    elif "Z" in round:
        # let's win
        return get_score(find_play(round[0], 1))
    else:
        # how'd we get here?
        raise NotImplementedError


def get_score(round):
    result = score_dict[round].copy()
    result[0] += ord(round[0]) - 64  # A-1
    result[1] += ord(round[2]) - 87  # X-1
    return result


for line in temp_input:
    elf_round, hum_round = get_score(line)
    elf_score += elf_round
    hum_score += hum_round

print(elf_score, hum_score)

elf_score = 0
hum_score = 0

for line in temp_input:
    elf_round, hum_round = force_round(line)
    elf_score += elf_round
    hum_score += hum_round

print(elf_score, hum_score)
