import re

move_re = r'move\s+(\d+)\s+from\s+(\d+)\s+to\s+(\d+)\s*'

stacks = []


def process_move(move, multimove=False):
    qty, src, dst = move

    while qty > 0:
        if multimove:
            # print(f'need to multimove {qty} from {src} to {dst}')
            tmp_buf = []
            while qty > 0:
                tmp_buf.append(stacks[src - 1].pop(0))
                qty -= 1
            tmp_buf.reverse()
            stacks[dst - 1].reverse()
            stacks[dst - 1].extend(tmp_buf)
            stacks[dst - 1].reverse()
            # print(stacks)
        else:
            # print(f'need to move {qty} from {src} to {dst}')
            tmp = stacks[src - 1].pop(0)
            stacks[dst - 1].insert(0, tmp)
            qty -= 1
            # print(stacks)


with open('../inputs/day5.txt') as puzzle_input:
    for line in puzzle_input:
        if line[0] == 'm':
            process_move(list(map(int, list(re.match(move_re, line).groups()))))
        elif '[' in line:
            for i in range(1, len(line), 4):
                idx = int((i - 1) / 4)
                if len(stacks) < idx + 1:
                    stacks.append([])
                if len(line[i].strip()) > 0:
                    stacks[idx].append(line[i])
        elif len(line.strip()) == 0:
            # print(stacks)
            pass

# print(stacks)
for stack in stacks:
    print(stack[0], end='')

print('\n')
stacks = []
with open('../inputs/day5.txt') as puzzle_input:
    for line in puzzle_input:
        if line[0] == 'm':
            process_move(list(map(int, list(re.match(move_re, line).groups()))), True)
        elif '[' in line:
            for i in range(1, len(line), 4):
                idx = int((i - 1) / 4)
                if len(stacks) < idx + 1:
                    stacks.append([])
                if len(line[i].strip()) > 0:
                    stacks[idx].append(line[i])
        elif len(line.strip()) == 0:
            # print(stacks)
            pass

# print(stacks)
for stack in stacks:
    print(stack[0], end='')
