f = open("../inputs/day3.txt", "r")
txt = f.read()
temp_input = txt.splitlines()


def get_letter_val(letter):
    if letter.islower():
        return ord(letter) - 96  # a-z is normally 97~122. remap to 1~26
    else:
        return ord(letter) - 38  # A~Z is normally 65~90, remap to 27~52


dupe_val_sum = 0
for line in temp_input:
    half = int(len(line)/2)
    left = set(line[:half])
    right = set(line[half:])

    dupe = left.intersection(right).pop()
    dupe_val_sum += get_letter_val(dupe)

print(dupe_val_sum)


badge_val_sum = 0
for i in range(0, len(temp_input), 3):
    first = set(temp_input[i])
    second = set(temp_input[i+1])
    third = set(temp_input[i+2])
    badge_val_sum += get_letter_val(set.intersection(first, second, third).pop())

print(badge_val_sum)
