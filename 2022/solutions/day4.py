f = open("../inputs/day4.txt", "r")
txt = f.read()
temp_input = txt.splitlines()

overlaps = 0
for line in temp_input:
    assignments = line.split(',')
    left_assign = list(map(int, assignments[0].split('-')))
    right_assign = list(map(int, assignments[1].split('-')))

    # left in right check
    if left_assign[0] >= right_assign[0] and left_assign[1] <= right_assign[1]:
        overlaps += 1
        # print(f'{line}: left fits in right')
        continue

    # right in left check
    if right_assign[0] >= left_assign[0] and right_assign[1] <= left_assign[1]:
        overlaps += 1
        # print(f'{line}: right fits in left')
        continue

    # print(f'{line}: no matches!')
print(overlaps)

overlaps = 0
for line in temp_input:
    assignments = line.split(',')
    left_assign = list(map(int, assignments[0].split('-')))
    right_assign = list(map(int, assignments[1].split('-')))

    # left in right check
    if right_assign[0] <= left_assign[0] <= right_assign[1]:
        overlaps += 1
        print(f'{line}: left low fits in right')
        continue

    # right in left check
    if right_assign[0] <= left_assign[1] <= right_assign[1]:
        overlaps += 1
        print(f'{line}: left high fits in right')
        continue

    # left in right check
    if left_assign[0] <= right_assign[0] <= left_assign[1]:
        overlaps += 1
        print(f'{line}: right low fits in left')
        continue

    # right in left check
    if left_assign[0] <= right_assign[1] <= left_assign[1]:
        overlaps += 1
        print(f'{line}: right high fits in left')
        continue

    print(f'{line}: no matches!')


print(overlaps)
