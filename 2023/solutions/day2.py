import math

f = open("../inputs/day2.txt", "r")
txt = f.read()
temp_input = txt.splitlines()

game_color_dict = {}

for line in temp_input:
    stuff = line.split(':')
    game_num = stuff[0].lstrip('Game ')
    draws = stuff[1].strip().split(';')
    game_color_dict[game_num] = {}
    for draw in draws:
        for colors in draw.strip().split(','):
            split = colors.strip().split(' ')
            count = split[0].strip()
            color = split[1].strip()
            # print(count, color)
            if color not in game_color_dict[game_num].keys():
                game_color_dict[game_num][color] = int(count)
            else:
                if int(game_color_dict[game_num][color]) < int(count):
                    game_color_dict[game_num][color] = int(count)

games_valid = []
for game, color_counts in game_color_dict.items():
    if color_counts['red'] <= 12 and color_counts['green'] <= 13 and color_counts['blue'] <= 14:
        games_valid.append(int(game))
print(sum(games_valid))

game_powers = []
for game, color_counts in game_color_dict.items():
    game_powers.append(math.prod(color_counts.values()))
print(sum(game_powers))
