import re

number_re = r'\D*(\d?)(?:.*)(\d).*$'
three_replace_dict = {
    "one": "1",
    "two": "2",
    "six": "6",
}
four_replace_dict = {
    "four": "4",
    "five": "5",
    "nine": "9"
}
five_replace_dict = {
    "three": "3",
    "seven": "7",
    "eight": "8",
}

three_back_replace_dict = {
    "eno": "1",
    "owt": "2",
    "xis": "6",
}

four_back_replace_dict = {
    "ruof": "4",
    "evif": "5",
    "enin": "9"
}

five_back_replace_dict = {
    "eerht": "3",
    "neves": "7",
    "thgie": "8",
}

p1_calibration_sum = 0
p2_calibration_sum = 0


def preparse_line(in_line: str):
    # print(in_line)
    one_found = False
    two_found = False
    for pos, ch in enumerate(in_line):
        for k, v in three_replace_dict.items():
            if k in in_line[pos:pos + 3]:
                in_line = f"{in_line[:pos]}{v}{in_line[pos + 3:]}"
                one_found = True
                break
        if one_found:
            break
        for k, v in four_replace_dict.items():
            if k in in_line[pos:pos + 4]:
                in_line = f"{in_line[:pos]}{v}{in_line[pos + 4:]}"
                one_found = True
                break
        if one_found:
            break
        for k, v in five_replace_dict.items():
            if k in in_line[pos:pos + 5]:
                in_line = f"{in_line[:pos]}{v}{in_line[pos + 5:]}"
                one_found = True
                break
        if one_found:
            break

    # print(in_line)
    in_line = in_line[::-1]
    for pos, ch in enumerate(in_line):
        for k, v in three_back_replace_dict.items():
            if k in in_line[pos:pos + 3]:
                in_line = f"{in_line[:pos]}{v}{in_line[pos + 3:]}"
                two_found = True
                break
        if two_found:
            break
        for k, v in four_back_replace_dict.items():
            if k in in_line[pos:pos + 4]:
                in_line = f"{in_line[:pos]}{v}{in_line[pos + 4:]}"
                two_found = True
                break
        if two_found:
            break
        for k, v in five_back_replace_dict.items():
            if k in in_line[pos:pos + 5]:
                in_line = f"{in_line[:pos]}{v}{in_line[pos + 5:]}"
                two_found = True
                break
        if two_found:
            break
    # print(in_line[::-1])
    return in_line[::-1]


with open('../inputs/day1.txt', 'r') as file:
    for line in file:
        matches = re.match(number_re, line)
        first, second = matches.groups()
        if len(first) == 0:
            first = second
        # print(f"{first}{second}")
        p1_calibration_sum += int(f"{first}{second}")
    print(p1_calibration_sum)

with open('../inputs/day1.txt', 'r') as file:
    for line in file:
        prep = preparse_line(line.strip())
        matches = re.match(number_re, prep)
        first, second = matches.groups()
        if len(first) == 0:
            first = second
        # print(f"{first}{second}: {line.strip()}/{prep}")
        p2_calibration_sum += int(f"{first}{second}")

    print(p2_calibration_sum)
